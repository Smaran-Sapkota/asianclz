$(document).ready(function(){
    mtSticky();
    $(window).scroll(mtSticky);
    function mtSticky() {
        var height = $(window).scrollTop(), stickyHeight = $('.header-section').outerHeight();
        $('body').removeClass('mt-sticky');
        if (height > stickyHeight) {
            $('body').addClass('mt-sticky');

        } else {
            $('body').removeClass('mt-sticky');
        }
    }	
    // main menu toggle js
    $('.menu-toggle').click(function () {
        $('.menu-wrap').toggleClass('on');
        $(this).toggleClass('active');
        $('.top-header').toggleClass('menu-on');
    });


   


    //jQuery for single course-page collapse the navbar on scroll
    $(window).scroll(function () {
        var bannerHeight = $("#inner-banner").height();
        var headerHeight = $('.top-header').outerHeight();
        var totalHeight =  bannerHeight + headerHeight;

        if ($(".navbar").offset().top > totalHeight) {
            $(".navbar-fixed-top").css({
                "top": headerHeight + 'px'
            }).addClass("top-nav-collapse");
        } else {
            $(".navbar-fixed-top").removeAttr('style').removeClass("top-nav-collapse");
        }
    });

    //jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function () {
        $('a.page-scroll').bind('click', function (event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - $(".navbar-fixed-top").outerHeight()
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });
   
// banner owl slider
    $("#slider-lists, #facility-lists, #testimonial-lists").owlCarousel({
		nav: false,
		dots: true,
		items: 1,
	    autoplay:true,
		autoPlaySpeed: 3000,
		loop: true
    });
    $("#news-lists").owlCarousel({
        nav: false,
        dots: true,
        items: 2,
        autoplay: true,
        autoPlaySpeed: 3000,
        loop: true
    });
    $("#event-lists").owlCarousel({
        nav: false,
        dots: true,
        items: 4,
        autoplay:false,
        autoPlaySpeed: 3000,
        loop: true,
        responsive:{
        0:{
            items:1,
            nav:true
        },
        480:{
            items:2,
            nav:false
        },
        900:{
            items:3,
            nav:true,
            loop:false
        },
        1200:{
            items:4,
            nav:true,
            loop:false
        }
    }
    });
    $("#activity-lists").owlCarousel({
        nav: false,
        dots: true,
        items: 2,
        autoplay: false,
        autoPlaySpeed: 3000,
        loop: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            480: {
                items: 2,
                nav: false
            }
        }
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');


       $(" #partners-lists").owlCarousel({
        nav: false,
        dots: true,
        items: 5,
        autoplay:true,
        autoPlaySpeed: 3000,
        loop: true,
        responsive:{
        0:{
            items:1,
            nav:true
        },
        480:{
            items:2,
            nav:false
        },
        900:{
            items:3,
            nav:true,
            loop:false
        },
        1200:{
            items:5,
            nav:true,
            loop:false
        }
    }
    });
    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');

    $("#alumni-voice").owlCarousel({
        nav: false,
        dots: true,
        items: 3,
        autoplay: true,
        autoPlaySpeed: 3000,
        loop: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            480: {
                items: 2,
                nav: false
            },
            991:{
                items: 3,
                nav: false
            }
        }
    });
    $(".owl-prev").html('<i class="fas fa-long-arrow-alt-left"></i>');
    $(".owl-next").html('<i class="fas fa-long-arrow-alt-right"></i>');

    // notification section js accordian
    $('.mt-notifications li h3').click(function () {
        $(this).next('.mt-content').slideToggle();
        $(this).toggleClass('active');
        $('.mt-notifications li h3').not(this).next('.mt-content').slideUp();
        $('.mt-notifications li h3').not(this).removeClass('active');

    });

    $('.info-accordian p').hide();
    $('.info-accordian  h4').click(function () {
        $(this).siblings('p').slideToggle();
        $(this).toggleClass('active');
        $('.info-accordian h4').not(this).siblings('p').slideUp();

    });

    // current page active for about us page
    let url = window.location.href;
    $('.nav-sidebar ul li a').each(function () {
        if (this.href === url) {
            $(this).addClass('active');
        }
        else{
            $(this).removeClass('active');
        }
    });

   
});


$('.faq-lists li h6').click(function(){
    $(this).toggleClass('on');
    $(this).next('.faq-answer').slideToggle();
    $('.faq-lists li h6').not(this).next('.faq-answer').slideUp();
    $('.faq-lists li h6').not(this).removeClass('on');
});

/* Local Storage cookies */
$(document).ready(function(){
	
	  
	if ( sessionStorage.getItem("firstLoad") === null) 
	{
		 $("#overlay").modal('show');
		   sessionStorage.setItem('firstLoad',1);
  
	}else if( sessionStorage.getItem("firstLoad") ==1){
		  	$('#overlay').modal('hide');
		// localStorage.removeItem(firstLoad);
	}
   //  if(localStorage.getItem('firstLoad') == 0)
   //  {
	  //   $("#overlay").modal('show');
	  //    localStorage.setItem('firstLoad',1);
   //  }else
   //  {
   //  	$('#overlay').modal('hide');
		 // // localStorage.setItem('popState','null');
   //  }
});

// sticky sidebar on scroll
if (jQuery(window).width() >= 960 && $('#sidebar').length) {
    var a = new StickySidebar('#sidebar', {
        topSpacing: 60,
        bottomSpacing: -20,
        containerSelector: '.container',
        innerWrapperSelector: '.sidebar-inner'
    });

}

    $(".floatinglikebox").hover(function() {
        $(this).stop().animate({right: "0"}, "medium");}, 
        function() {
            $(this).stop().animate({right: "-250"}, "medium");}, 
            500);





(function() {

    //variables
    var $window = $(window),
        $section = $('section'),
        $scrollPosition = $window.scrollTop(),
        $sectionHeights = [];


    //will calculate each section heights and store them in sectionHeights[] array
    function resizeSectionHeights() {

        $section.each(function (i) {
            $sectionHeights[i] = $(this).outerHeight();
        });

    }


    /*will calculate current scroll position. If it's between the top and bottom of a section, 
      the tab containing an href value of that section id will have the .current class.*/
    function applyCurrentState() {

        $scrollPosition = $window.scrollTop();
        var topHeaderHeight = $('.top-header').outerHeight() + $('.course-nav').outerHeight()

        $section.each(function (i) {    //we indent i at each section count, so we can find it's height in sectionHeight[]

            var $anchor = $(".nav.navbar-nav a[href=#" + $(this).attr('id') + "]"),
                $sectionTop = $(this).offset().top - 1 - topHeaderHeight,                    // -1 get rid of calculation errors
                $sectionBottom = $sectionTop + $sectionHeights[i] - 1;    // -1 get rid of calculation errors

            if ($scrollPosition >= $sectionTop && $scrollPosition < $sectionBottom) {

                $anchor.addClass('current');

            } else {

                $anchor.removeClass('current');
            }
        });
    }


    //binding events
    $window.resize(resizeSectionHeights);
    $window.scroll(applyCurrentState);


    //initialization
    resizeSectionHeights();
})()

String.prototype.ucFirst = function () {
        return this.charAt(0).toUpperCase() + this.slice(1);
    };

    var config = {
        error: {
            'element': 'span.wpcf7-not-valid-tip',
            'messagePlaceholder': ':field is required.'
        },
    };
// Loads the YouTube IFrame API JavaScript code.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
// Inserts YouTube JS code into the page.
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

// onYouTubeIframeAPIReady() is called when the IFrame API is ready to go.
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'uwxwu1rpgXw', // the ID of the video (mentioned above)
        playerVars: {
            autoplay: 1, // start automatically
            controls: 0, // don't show the controls (we can't click them anyways)
            modestbranding: 1, // show smaller logo
            mute: 1, // sound mute
            loop: 1, // loop when complete
            playlist: 'kNizPk7xBbs' // required for looping, matches the video ID
        }
    });
}